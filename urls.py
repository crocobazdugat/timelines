from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #url(r'^import-messages/(?P<pk>\d+)/?$', views.import_messages, name='import_messages')
]
