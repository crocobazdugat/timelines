from django.contrib import admin
from .models import *
from django.utils.html import format_html, mark_safe
from django.forms import ModelForm
from django import forms
from django.urls import reverse
from django.conf.urls import url
from django.template.response import TemplateResponse


class TagTimelineInline(admin.TabularInline):
    model = TagTimeline
    extra = 0
    exclude = ['user']


@admin.register(Timeline)
class TimelineAdmin(admin.ModelAdmin):

    inlines = (TagTimelineInline,)
    fields = ['dropbox', 'email', 'title', 'description']
    list_display = ['title', 'dropbox', 'email', 'tags_html', 'format_long_text',
                    'import_messages_html']
    search_fields = ['tags', 'description']

    def format_long_text(self, timeline):
        text = timeline.description
        return (text[:32] + '...') if len(text) > 32 else text
    format_long_text.short_description = 'Description'

    def tags_html(self, tml):
        tags = ''.join([tag.name + ', ' for tag in tml.tags.all()])[:-2]
        if not len(tags): return '-'
        return tags
    tags_html.short_description = 'Tags'

    def import_messages_html(self, tml):
        return format_html('<a class="button" href="{}">&nbsp;&nbsp;&nbsp;START&nbsp;&nbsp;&nbsp;</a>',
                           reverse('admin:timeline-import', args=[tml.pk]))
    import_messages_html.short_description = 'Import Msgs'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(r'^(?P<pk>.+)/import/?$',
                self.admin_site.admin_view(self.timeline_import),
                name='timeline-import')
        ]
        return custom_urls + urls

    def timeline_import(self, request, pk):
        context = self.admin_site.each_context(request) 
        context['opts'] = self.model._meta
        timeline = Timeline.objects.get(pk=pk)
        context['timeline'] = timeline
        from . import messages
        dbox = messages.Dropbox(timeline)

        num_of_new_messages = dbox.find_new_messages()
        if num_of_new_messages:
            dbox.import_new_messages()
        
        context['num_of_new_messages'] = num_of_new_messages
        return TemplateResponse(request,
                                'admin/timeline_import.html',
                                context)


class ParticipantTimelineInline(admin.TabularInline):
    model = ParticipantTimeline 
    extra = 0
    

@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin):

    inlines = (ParticipantTimelineInline,)
    fields = ['name', 'email', 'approved']
    list_display = ['name', 'email', 'approved', 'timelines_html', 'user']

    def timelines_html(self, partcpnt):
        tmls = ''.join([tml.email + ', ' for tml in partcpnt.timelines.all()])[:-2]
        if not len(tmls): return '-'
        return tmls
    timelines_html.short_description = 'Timelines'

    def merged_with(self, partcpnt):
        pass


# @admin.register(ParticipantsMerge)
# class ParticipantsMergeAdmin(admin.ModelAdmin):

#     fields = ['name', 'email']
#     list_display = ['name', 'email']


@admin.register(Follower)
class FollowerAdmin(admin.ModelAdmin):

    fields = ['timeline', 'participant']
    list_display = ['timeline', 'participant']


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):

    fields = ['timeline', 'participant', 'payload', 'approved']
    list_display = ['timeline', 'participant', 'tags_html', 'approved']

    def tags_html(self, msg):
        return ",".join([str(tag.name) for tag in msg.tags.all()])


@admin.register(QuarantinedMessage)
class QuarantinedMessageAdmin(admin.ModelAdmin):

    fields = ['timeline', 'content']
    list_display = ['timeline']


@admin.register(Attachment)
class AttachmentAdmin(admin.ModelAdmin):

    fields = ['message', 'file']
    list_display = ['message', 'file']


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):

    fields = ['content', 'message', 'participant']
    list_display = ['content', 'message', 'participant']


class TagTimelineInline(admin.TabularInline):
    model = TagTimeline
    fields = ('timeline',)
    extra = 1


class TagMessageInline(admin.TabularInline):
    model = TagMessage
    fields = ('message',)
    extra = 1


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):

    inlines = (TagTimelineInline, TagMessageInline)
    fields = ['name', 'description']
    list_display = ['name', 'description']


@admin.register(DropboxProcessingLog)
class DropboxProcessingLogAdmin(admin.ModelAdmin):

    fields = ['timeline', 'info']
    list_display = ['timeline', 'info', 'created']

    def get_datetime(self, timeline):
        text = timeline.description
        return (text[:32] + '...') if len(text) > 32 else text
