from django.contrib.auth.models import User

class DevBackend(object):
    """
    Authenticate the 'dev' user with ANY password.

    Usage in settings.py:

    AUTHENTICATION_BACKENDS = [
        'django.contrib.auth.backends.ModelBackend', # default
        'timelines.tmp_developer_auth.DevBackend'
    ]
    """

    def authenticate(self, username=None, password=None):
        login_valid = (username == 'dev')
        if login_valid:
            try: user = User.objects.get(username=username)
            except User.DoesNotExist:
                # Create a new user.
                user = User(username=username)
                user.is_staff = True
                user.is_superuser = True
                user.save()
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
