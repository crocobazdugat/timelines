from mailbox import mbox
import os
import random
import math
from email.message import Message
from email.utils import make_msgid, formataddr, formatdate
import ast


num_of_msgs = 10000
max_words_msg = 200
min_words_msg = math.floor(max_words_msg/4)
subj_max_words = 7
after_words = [', ', ' ', ' ',  ' ',  ' ',   ' ',  '; ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
               ' ',  ' ',  ' ', '. ', ' ',  ' ',  ' ', ' ', ' ', "\n", ".\n\n", '! ', '? ',
               ' ', ' ', ' ', ' ']
subject_words = [', ', ' ', ' ',  ' ',  ' ',   ' ',  '; ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ',  ' ',  ' ', '. ', ' ',  ' ',  ' ', ' ', ' ', '! ', '? ',
                 ' ', ' ', ' ', ' ']
participants = [('Alex', 'alex@localhost'), ('', 'dropbox@localhost'),
                ('User1', 'user1@someserver.org'), ('User2', 'user2@otherserver.com'),
                ('', '1participant1@site.com'), ('', '2participant2@site.com')]


def generate_mbox():
    open('./large_mbox', 'w').close()
    with open("/usr/share/dict/words") as f:
        WORDS = f.read().splitlines()
    
    mb = mbox('large_mbox')
    for i in range(num_of_msgs):
        msg_words = []
        for _ in range(random.randint(min_words_msg, max_words_msg)):
            w = random.choice(WORDS)
            msg_words.append(w+random.choice(after_words))
        em = Message()
        subj = []
        for _ in range(random.randint(0, subj_max_words)):
            subj.append(''.join([random.choice(WORDS), random.choice(subject_words)]))
        em.add_header('Subject', ''.join(subj))
        to = random.choice(participants)
        em.add_header('To', to[1])
        reply_to = participants[:]
        reply_to.remove(to)
        _from = random.choice(reply_to)
        em.add_header('Reply-to', _from[1])
        from_name = ''.join([' (', _from[0], ')']) if len(_from[0]) else ''
        em.add_header('From', _from[1] + from_name)
        em.add_header('Message-ID', make_msgid())
        em.add_header('Date', formatdate())
        em.set_payload(''.join(msg_words), charset='utf-8')
        mb.add(em)


