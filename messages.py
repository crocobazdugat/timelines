from .models import *
import os
from stat import S_ISDIR as is_dir, ST_MODE
from mailbox import mbox, Maildir
from email.utils import parsedate
from time import time, mktime


class Dropbox:

    def __init__(self, timeline):
        self.timeline = timeline
        self.path = timeline.dropbox
        self.email = timeline.email
        self.mailbox = self.get_mailbox()

    def get_mailbox(self):
        mode = os.stat(self.path)[ST_MODE]
        if not is_dir(mode):
            return mbox(self.path, create=False)
        else:
            return Maildir(self.path, create=False)

    def find_new_messages(self):
        mbox = self.mailbox
        # get latest email message in db
        lmsg = Message.objects.filter(pk=self.timeline.id, is_email=True).order_by('-_date').first()
        if not lmsg: # empty table
            return len(mbox)

        last_time_in_db = lmsg._date if lmsg._date else lmsg.created
        last_timestamp_in_db = last_time_in_db.timestamp()

        num_of_new_messages = 0
        self.first_new_idx = None
        # from most recent to earlier
        for i in range(len(mbox)-1, -1, -1):
            msg = mbox.get(i)
            if msg.get('date'):
                msg_time = mktime(parsedate(msg.get('date')))
            else:
                msg_time = self._parse_header_date(msg)

            if not msg_time: continue # dateless message
            
            if msg_time > last_timestamp_in_db:
                num_of_new_messages += 1
                self.first_new_idx = i
            else:
                break

        return num_of_new_messages

    def _parse_header_date(self, msg):
        header = msg.as_string(unixfrom=True).split(os.linesep)[0]
        header_date = ' '.join(header.split()[2:])
        return mktime(parsedate(header_date))

    def import_new_messages(self):
        mbox = self.mailbox
        for i in range(self.first_new_idx, len(mbox)): 
            email = mbox[i]
            msg = self._create_message(email)

    def _create_message(self, email):
        msg = Message()
        msg.timeline = self.timeline
        msg.headers = str(email.items())
        msg.approved = True
        msg.is_email = True
        msg._message_id = email.get('Message-Id')
        msg._subject = email.get('Subject')
        msg._from = email.get('From')
        msg._to = email.get('To')
        msg._date = email.get('Date')
        if email.is_multipart():
            self._parse_mime(msg, email)
        else:
            msg.payload = email.get_payload()
        return msg
            
    def _parse_mime(self, msg, email):
        for part in email.walk():
            maintype = part.get_content_maintype()
            if maintype == 'multipart':
                continue
            elif maintype == 'text':
                msg.payload = part.get_payload()
            else:
                mime_type = part.get_content_type()
                name = part.get_param('name')
                
