var gulp = require('gulp');
//cache = require('gulp-cached');

gulp.task('default', ['copy-modules']);

gulp.task('copy-modules', function() {
  try {
    gulp.src(['node_modules/vis/dist/**'])
    .pipe(gulp.dest('vis/'));
  }
  catch (e) {
    return -1;
  }
  return 0;
});

gulp.task('test', function() {
  console.log('test');
});
