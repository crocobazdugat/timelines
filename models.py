from django.db import models
from django.conf import settings # for AUTH_USER_MODEL


class Timeline(models.Model):
    """Timeline for a mailbox file.
    A path to a file(detected as 'mbox' format) or a dir('maildir' format).
    Mailbox must be writetable, the app should remove message from it and store in its db.
    Local echo mailbox can be set in Postfix using 'always_bcc' option or similar.
    """
    dropbox = models.CharField(
        max_length=1023,
        help_text="Path to a mailbox('mbox' file or 'maildir' folder)")
    email = models.EmailField(
        help_text="Email to which participants should send their messages",
        max_length=255,
        unique=True)
    title = models.CharField(
        max_length=255,
        help_text="Title for a timeline",
        unique=True)
    description = models.TextField(max_length=1023, null=True, blank=True) 
    # number_of_messages = models.IntegerField(
    #     help_text="Number of messages in timeline",
    #     default=0)
    tags = models.ManyToManyField('Tag', through="TagTimeline")
    # This is to find new messages to import from a dropbox by date
    #last_message_date = models.DateTimeField('Last message date')
    being_processed = models.BooleanField(
        default=False, 
        help_text='Lock indicating that processing of a dropbox is going on, initiated by user or cronjob.')
    processing_started_date = models.DateTimeField(
        'when dropbox processing was started',
        null=True,
        help_text='Date the dropbox processing was started by user or cronjob. Reset if exceeds some limit.',)
    created = models.DateTimeField('creation date', auto_now_add=True)

    class Meta:
        permissions = (
            ("view_timeline", "Can view timeline"),
            ("tag_timeline", "Can tag timeline")
        )

    def __str__(self):
        return self.title


class ParticipantsMerge(models.Model):
    """For a manual merge of participant rows that 
    supposedly represent one real participant."""
    name = models.CharField(max_length=255)
    email = models.EmailField()
    created = models.DateTimeField('creation date', auto_now_add=True)


class Participant(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    merge = models.ForeignKey(ParticipantsMerge,
                              null=True,
                              blank=True,
                              on_delete=models.SET_NULL)
    approved = models.BooleanField(default=False) 
    timelines = models.ManyToManyField(Timeline,
                                       help_text="In which timelines participated.",
                                       through="ParticipantTimeline")
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             null=True,
                             blank=True,
                             on_delete=models.SET_NULL)
    created = models.DateTimeField('creation date', auto_now_add=True)
    updated = models.DateTimeField('last update date', auto_now=True)

    class Meta:
        permissions = (
            ("approve_participant", "Can approve participant"),
        )

    def __str__(self):
        return ''.join([self.name, ' ', self.email])


class ParticipantTimeline(models.Model):
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE)
    timeline = models.ForeignKey(Timeline, on_delete=models.CASCADE)
    date_joined = models.DateTimeField(auto_now_add=True)


class Follower(models.Model):
    timeline = models.ForeignKey(Timeline,
                                 null=False,
                                 blank=False,
                                 on_delete=models.CASCADE)
    participant = models.ForeignKey(Participant,
                                    null=False,
                                    blank=False,
                                    on_delete=models.CASCADE)

    class Meta:
        permissions = (
            ("add_timeline_follower", "Can add timeline follower"),
        )


class Message(models.Model):
    timeline = models.ForeignKey(Timeline, on_delete=models.CASCADE) 
    participant = models.ForeignKey(Participant,
                                    null=True,
                                    on_delete=models.SET_NULL,
                                    related_name="sender")
    # All headers as a Python literal list of 2-tuples(key, value) from
    # email.Message.items() evaled by ast.literal_eval();
    headers = models.TextField(help_text='Message headers.')
    # Message content
    payload = models.TextField(help_text='Message text.')
    tags = models.ManyToManyField('Tag', through="TagMessage")
    approved = models.BooleanField(default=False)
    is_email = models.BooleanField(
        default=True,
        help_text='If True then "content" field is a raw email message with headers, \
        without multipart payload(attachments).')
    is_multipart = models.BooleanField(default=False)
    # Headers
    _message_id = models.CharField('"Message-ID" header', max_length=995)
    _subject = models.CharField('"Message-ID" header', max_length=63)
    _from = models.CharField('"From" header', max_length=255, null=True)
    _to = models.CharField('"To" header', max_length=255, null=True)
    _date = models.DateTimeField('"Date" header, when composed by sender', null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        permissions = (
            ("approve_message", "Can approve message"),
            ("tag_message", "Can tag message")
        )


class QuarantinedMessage(models.Model):
    timeline = models.ForeignKey(Timeline, on_delete=models.CASCADE)
    content = models.TextField()
    created = models.DateTimeField('When it was retrieved from a dropbox or submitted by webform',
                                   auto_now_add=True)


class Attachment(models.Model):
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    file = models.FileField(upload_to='timeline_attachments/%Y/%m/%d/')


class Comment(models.Model):
    content = models.TextField()
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE)


class Tag(models.Model):
    name = models.CharField(max_length=127)
    description = models.TextField()

    def __str__(self):
        return self.name


class TagTimeline(models.Model):
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    timeline = models.ForeignKey(Timeline, on_delete=models.CASCADE)
    date_tagged = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             null=True,
                             blank=True,
                             on_delete=models.SET_NULL,
                             help_text="Which user tagged it.")


class TagMessage(models.Model):
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    date_tagged = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             null=True,
                             blank=True,
                             on_delete=models.SET_NULL,
                             help_text="Which user tagged it.")
    

class DropboxProcessingLog(models.Model): 
    info = models.TextField()
    timeline = models.ForeignKey(Timeline, on_delete=models.CASCADE)
    created = models.DateTimeField('processing date', auto_now_add=True)

    class Meta:
        permissions = (
            ("view_dropbox_processing_log", "Can view dropboxes processing log"),
        )
